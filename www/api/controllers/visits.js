var VisitsController = {
    index: function( req, res, next ) {
        db.Visit.findAll({
            where: {
                'uid': req.params.user_id
            },
            limit: (req.query.count ? req.query.count : null)
        })
        .then(function(results) {
            res.send(results);
        });
    },
    show: function( req, res, next ) {
        db.Visit.find({
            where: {
                'id': req.params.id
            },
            limit: (req.query.count ? req.query.count : null)
        })
        .then(function(results) {
            try {
                res.send(results);
            }
            catch(e) {
                res.send(e);
            }
        });
    },
    create: function( req, res, next ) {
        return db.sequelize.transaction(function (t) {
            return db.Visit.create({
                uid: req.params.user_id,
                city: JSON.stringify(req.body)
            }, {transaction: t}).then(function (user) {
               res.send(user);
            });
        }).then(function (result) {
            // Transaction has been committed
            res.send(result);
        }).catch(function (err) {
            // Transaction has been rolled back
            res.send(err);
        });
    }
};
module.exports = VisitsController;