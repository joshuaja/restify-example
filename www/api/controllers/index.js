var html = '<h1>API Documentation</h1>' +
    '<table style="width:100%; border: 1px solid #333;">' +
    '<thead style="background-color: #eee;">' +
    '<tr>' +
    '<td>'+i18n.METHOD+'</td>' +
    '<td>'+i18n.PATH+'</td>' +
    '</tr>' +
    '</thead>' +
    '<tbody>%body<tbody>' +
    '</table>';
var body = '';
var routes = server.router.mounts;

var IndexController = {
    index: function( req, res, next ) {
        for (var key in routes) {
            if (routes.hasOwnProperty(key)) {
                var val = routes[key];
                console.log(val);
                body += '<tr><td>'+val.method+'</td>' + '<td>'+val.spec.path+'</td></tr>';
            }
        }
        html = html.replace('%body', body);

        res.writeHead(200, {
            'Content-Length': Buffer.byteLength(html),
            'Content-Type': 'text/html'
        });
        res.write(html);
        res.end();
    }
};
module.exports = IndexController;