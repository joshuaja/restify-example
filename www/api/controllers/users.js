var UsersController = {
    index: function( req, res, next ) {
        db.User.findAll(
            {
                limit: (req.query.count ? req.query.count : null)
            }
        )
        .then(function(results) {
            try {
                res.send(results);
            }
            catch(e) {
                res.send(e);
            }
        });
    },
    show: function( req, res, next ) {
        db.User.find(
        {
            where:
            {
                id: req.params.id
            }
        })
        .then(function(results) {
            try {
                res.send(results);
            }
            catch(e) {
                res.send(e);
            }
        });
    }
};
module.exports = UsersController;