var CitiesController = {
    index: function( req, res, next ) {
        db.City.findAll({
            where: {
                'state': req.params.state_id
            },
            limit: (req.query.count ? req.query.count : null)
        })
        .then(function(results) {
            try {
                res.send(results);
            }
            catch(e) {
                res.send(e);
            }
        });
    },
    show: function( req, res, next ) {
        db.City.find({
            where: {
                'state': req.params.state_id,
                'name' : req.params.id
            },
            limit: (req.query.count ? req.query.count : null)
        })
        .then(function(city) {
            if (req.query.radius === undefined) {
                try{
                    res.send(city);
                }
                catch(e) {
                    res.send(e);
                }
            }
            else {
                db.sequelize.query('SELECT *, geo_distance('+city.latitude+', '+city.longitude+', latitude, longitude) AS `distance` FROM cities HAVING `distance`<='+req.query.radius+' ORDER BY `distance` ASC'+(req.query.count ? " LIMIT " + req.query.count : "")+';').spread(function(cities) {
                    try {
                        res.send(cities);
                    }
                    catch(e) {
                        res.send(e);
                    }
                });
            }
        });
    }
};
module.exports = CitiesController;