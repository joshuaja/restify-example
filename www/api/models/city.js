module.exports = function(sequelize, DataTypes) {
    return sequelize.define('city',
    {
        name: DataTypes.STRING(40),
        state: DataTypes.STRING(2),
        latitude: DataTypes.DECIMAL(10,8),
        longitude: DataTypes.DECIMAL(11,8)
    },
    {
        timestamps: false
    });
};
