var config = require('../config/config');

if (!global.hasOwnProperty('db')) {
    var Sequelize = require('sequelize'),
        sequelize = null;

    sequelize = new Sequelize(config.db.name, config.db.user, config.db.password);
    global.db = {
        Sequelize: Sequelize,
        sequelize: sequelize,
        City:      sequelize.import(__dirname + '/city'),
        User:      sequelize.import(__dirname + '/user'),
        Visit:     sequelize.import(__dirname + '/visit')
    };

}

module.exports = global.db;