module.exports = function(sequelize, DataTypes) {
    return sequelize.define('user',
    {
        first_name: DataTypes.STRING(40),
        last_name: DataTypes.STRING(40)
    },
    {
        timestamps: false
    });
};
