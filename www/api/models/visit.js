module.exports = function(sequelize, DataTypes) {
    return sequelize.define('visit',
    {
        uid:
        {
            type: DataTypes.INTEGER,
            references: 'users',
            referencesKey: 'id'
        },
        city: DataTypes.STRING(255)
    },
    {
        timestamps: false
    });
};