var prefix ="v1";

module.exports = function( router) {
        // GET '/' => controller: 'index', action: 'index'
        router.get( prefix, 'index#index' );

        router.resources( 'states', {prefixRoute: prefix});
        router.resources( 'cities', {prefixRoute: prefix+'/states/:state_id'});
        router.resources( 'users', {prefixRoute: prefix});
        router.resources( 'visits', {prefixRoute: prefix+'/users/:user_id'});
};