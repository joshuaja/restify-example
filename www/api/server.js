// Module includes
var http      = require('http'),
    restify   = require('restify'),
    path      = require('path'),
    Router    = require('paper-router'),
    routes    = require('./routes'),
    db        = require('./models'),
    config    = require('./config/config');

// Private declarations
    port      = 80;

global.i18n = require('./config/i18n');
global.server = restify.createServer({
    name : config.name,
    version: config.version
});

server
    .use( function crossOrigin( req, res, next ) {
        res.header( "Access-Control-Allow-Origin", "*" );
        res.header( "Access-Control-Allow-Headers", "X-Requested-With" );
        return next();
    })
    .use(restify.fullResponse())
    .use(restify.acceptParser(server.acceptable))
    .use(restify.bodyParser())
    .use(restify.queryParser())
    .use(restify.CORS())
    .pre(restify.pre.sanitizePath());

// Routes are under the 'controllers' folder
var router = new Router( server, path.join( __dirname, '/controllers' ), routes);
global.router = router;

server.listen(port);