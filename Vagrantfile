# -*- mode: ruby -*-
# vi: set ft=ruby :

# Variables
host_name         = "challenge.spartz.local"         # Set name of hostname (also VirtualBox VM name)
server_ip         = "192.168.55.108"                 # Set static IP of server
server_memory     = "512"                            # Ram to reserve (MB)
server_timezone   = "CST"                            # Set the timezone

db_name           = "spartz"                         # MySQL database name
db_user           = "root"                           # MySQL database username
db_pass           = "root"                           # MySQL database password
db_file           = "db/spartz_coding_challenge.sql" # SQL dump file to import on machine provision (relative to /vagrant)

server_up_message = <<-"MSG"

Project: Restify Example

VM name: #{host_name}
ip address: #{server_ip}

MSG

# Vagrant
Vagrant.configure("2") do |config|
    config.vm.box = "precise32"
    config.vm.network "private_network", ip: server_ip
    config.ssh.shell = "bash -c 'BASH_ENV=/etc/profile exec bash'"
    config.vm.define host_name
    config.vm.hostname = host_name
    config.vm.post_up_message = server_up_message

    config.vm.provider :virtualbox do |vb|
        # Configure VM name
        vb.name = host_name

        # Set server memory
        vb.customize ["modifyvm", :id, "--memory", server_memory]

        vb.customize ["setextradata", :id, "VBoxInternal2/SharedFoldersEnableSymlinksCreate/v-root", "1"]

        # Set the timesync threshold to 10 seconds, instead of the default 20 minutes.
        # If the clock gets more than 15 minutes out of sync (due to your laptop going
        # to sleep for instance, then some 3rd party services will reject requests.
        vb.customize ["guestproperty", "set", :id, "/VirtualBox/GuestAdd/VBoxService/--timesync-set-threshold", 10000]

        # Prevent VMs running on Ubuntu to lose internet connection
        vb.customize ["modifyvm", :id, "--natdnshostresolver1", "on"]
        vb.customize ["modifyvm", :id, "--natdnsproxy1", "on"]
    end

    config.vm.provision :shell,
        :inline => <<-SCRIPT
        # Configure timezone
            echo -e "✔ Setting Timezone & Locale\n"
            ln -sf /usr/share/zoneinfo/#{server_timezone} /etc/localtime
            export LANGUAGE=en_US.UTF-8
            export LANG=en_US.UTF-8
            export LC_ALL=en_US.UTF-8
            locale-gen en_US.UTF-8 > /dev/null 2>&1
            dpkg-reconfigure locales > /dev/null 2>&1
            echo "export LANG=en_US.UTF-8" >> /home/vagrant/.bashrc && source /home/vagrant/.bashrc
            . /home/vagrant/.bashrc

        # Install base packages
            echo -e "✔ Updating packages list\n"
            sudo apt-get -qq update
            echo -e "✔ Installing base packages\n"
            sudo apt-get -y install vim curl git > /dev/null 2>&1
            sudo apt-get -qq update

        # Install MySQL
            if ! which mysql &> /dev/null; then
                debconf-set-selections <<< 'mysql-server mysql-server/root_password password #{db_pass}'
                debconf-set-selections <<< 'mysql-server mysql-server/root_password_again password #{db_pass}'
                apt-get update > /dev/null 2>&1
                apt-get install -y mysql-server --fix-missing --fix-broken > /dev/null 2>&1
            fi

            Q1="CREATE DATABASE IF NOT EXISTS \"#{db_name}\";"
            Q2="GRANT ALL ON *.* TO '#{db_user}'@'localhost';"
            Q3="FLUSH PRIVILEGES;"
            Q4="use \"#{db_name}\";"
            [ -f "/vagrant/#{db_file}" ] && Q5="source \"/vagrant/#{db_file}\";" || Q5=""
            SQL="${Q1}${Q2}${Q3}${Q4}${Q5}"
            mysql -u#{db_user} -p#{db_pass} -e "$SQL"

        # Install NodeJS and npm
            echo -e "✔ Installing NodeJS and npm\n"
            sudo apt-get install -y python g++ make > /dev/null 2>&1
            echo prefix=/home/vagrant/.node > /home/vagrant/.npmrc
            curl -sL https://deb.nodesource.com/setup | sudo bash - > /dev/null 2>&1
            sudo apt-get install -y nodejs > /dev/null 2>&1
            npm install -g npm@latest > /dev/null 2>&1
            echo "export NODE_PATH=/usr/lib/nodejs:/usr/lib/node_modules:/usr/share/javascript:/home/vagrant/.node/lib/node_modules" >> /home/vagrant/.bashrc && source /home/vagrant/.bashrc
            echo "export PATH=$PATH:/home/vagrant/.node/bin" >> /home/vagrant/.bashrc

            sudo apt-get install -y libcap2-bin > /dev/null 2>&1
            sudo setcap cap_net_bind_service=+ep /usr/bin/nodejs

        # Install Forever
            if ! which forever &> /dev/null; then
                echo -e "✔ Installing Forever\n"
                npm install -g forever > /dev/null 2>&1
            fi
        SCRIPT

    # Run custom startup scripts
    config.vm.provision "shell",
        path: "startup.sh",
        run: "always"

end