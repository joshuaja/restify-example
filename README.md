# Restify Example - Cities and Users

## Overview

A RESTful API for interacting with City and User data based on Node.JS, MySQL, Restify, Forever and Sequelize - all wrapped up in a nice little Vagrant development VM for ease of distribution!

Distance between cities is based on the Haversine formula (http://en.wikipedia.org/wiki/Haversine_formula).

## Dependencies

Make sure Vagrant version >= 1.7 (www.vagrantup.com) and VirtualBox (www.virtualbox.org) are installed on your local machine before proceeding

## Usage

To start the vm, run 'vagrant up' from within the project folder (the folder with Vagrantfile in the root:
```
vagrant up
```

The first time will take a few minutes as the VM needs to be provisioned. When provisioning is complete, Vagrant will run the startup.sh script which downloads any npm modules into the app directory and starts the app via an npm task.

After the VM is provisioned, you can access files within the `www` folder by navigating to http://192.168.55.108 in your local web browser.

### Configuring your hosts file to point to the VM

If you prefer to add an entry to your hosts file, the VM host name is configured at http://challenge.spartz.local

On a Mac/Linux machine, in Terminal:

```
sudo nano /etc/hosts/
```
and add the following to your hosts file:

```
192.168.55.108  challenge.spartz.local
```

### SSH access to the VM

If you need to ssh into the vagrant box, `cd` to the project folder and type `vagrant ssh` in a terminal window:
```
cd [project folder]
vagrant ssh
```

## API Endpoints

| Method   | Path                             | Description                                      | Example
| -------- | -------------------------------- | ------------------------------------------------ | -------
| GET	   | v1                               | GET API Endpoint documentation (in progress)     | http://192.168.55.108/v1
| GET	   | v1/states/:state_id/cities       | GET all cities within a state                    | http://192.168.55.108/v1/states/IL/cities
| GET	   | v1/states/:state_id/cities/:id   | GET a city within a state                        | http://192.168.55.108/v1/states/IL/cities/Chicago
| GET	   | v1/states/:state_id/cities/:id?radius   | GET cities within x mile radius of a city        | http://192.168.55.108/v1/states/IL/cities/Chicago?radius=100
| GET	   | v1/users                         | GET all users                                    | http://192.168.55.108/v1/users
| GET	   | v1/users/:id                     | GET a user based on their user id                | http://192.168.55.108/v1/users/4
| GET	   | v1/users/:user_id/visits         | GET all visits for a particular user id          | http://192.168.55.108/v1/users/4/visits
| GET	   | v1/users/:user_id/visits/:id     | GET a particular visit                           | http://192.168.55.108/v1/users/4/visits/9
| POST     | v1/users/:user_id/visits         | CREATE a visit that is associated with a user id | http://192.168.55.108/v1/users/4/visits (POST must contain a body and contain the header Content-Type: "application/json")

### Limiting record count

The number of records that are returned can be limited by inserting a count into the querystring.

For example:

http://192.168.55.108/v1/states/IL/cities/Chicago?radius=76&count=4

http://192.168.55.108/v1/users?count=2